# -*- coding: utf-8 -*-

{
    'name': 'Ship Management',
    'version': '10.0.1.0',
    'category': '',
    'author': 'Aktiv Software',
    'website': 'www.aktivsoftware.com',
    'summary': '',
    'description': '',
    'depends': [
                'sale',
                'stock',
                'purchase',
                'hr_expense',
    ],
    'data': [
        'views/hr_expense_view.xml',
        'views/res_partner_view.xml',
        'views/ship_view.xml',
        'views/ship_stay_view.xml',
        'views/sale_order_view.xml',
        'views/purchase_view.xml',
        'views/terms_condition_view.xml',
        'views/account_invoice_view.xml',
        'views/stock_picking_view.xml',
        'report/picking_delivery_slip.xml',
        'report/quotation_report.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
