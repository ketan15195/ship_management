# -*- coding: utf-8 -*-

from odoo import fields, models


class Account_Invoice(models.Model):
	_inherit = "account.invoice"

	ship_stay_ids = fields.One2many('ship.stay','account_invoice_id')