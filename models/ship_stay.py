# -*- coding: utf-8 -*-

from odoo import fields, models


class ShipStay(models.Model):
    _name = "ship.stay"

    name = fields.Char(string="Name",required=True)
    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")
    notes = fields.Html(string="Notes")
    ship_id = fields.Many2one('res.partner',domain=[('is_ship','=', True)], required=True)
    sale_order_id = fields.Many2one('sale.order')
    purchase_order_id = fields.Many2one('purchase.order')
    stock_picking_id = fields.Many2one('stock.picking')
    account_invoice_id = fields.Many2one('account.invoice')
    hr_expense_id = fields.Many2one('hr.expense')