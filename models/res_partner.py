# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _inherit = "res.partner"

    def _compute_ship_stay_count(self):
        for wizard in self:
            wizard.ship_stay_count = self.env['ship.stay'].search_count([('ship_id', '=', wizard.id)])

    def _compute_ship_expense_count(self):
        for wizard in self:
            wizard.ship_expenses_count = self.env['hr.expense'].search_count([('ship_id', '=', wizard.id)])

    def _compute_outgoing_picking_count(self):
        for wizard in self:
             sale_order_ids = self.env['sale.order'].search([('ship_id', '=', wizard.id)])
             wizard.outgoing_picking_count = len(sale_order_ids.mapped('picking_ids'))

    def _compute_incoming_picking_count(self):
        for wizard in self:
             purchase_order_ids = self.env['purchase.order'].search([('ship_id', '=', wizard.id)])
             wizard.incoming_picking_count = len(purchase_order_ids.mapped('picking_ids'))

    def _compute_purchase_order_count(self):
        for wizard in self:
            wizard.purchase_order_count = self.env['purchase.order'].search_count([('ship_id', '=', wizard.id)])

    def _compute_ship_sale_order_count(self):
        for wizard in self:
            wizard.ship_sale_order_count = self.env['sale.order'].search_count([('ship_id', '=', wizard.id)])

    def _compute_account_invoice_count(self):
        for wizard in self:
            sale_order_ids = self.env['sale.order'].search([('ship_id', '=', wizard.id)])
            wizard.account_invoice_count = len(sale_order_ids.mapped('invoice_ids'))
    
    def _compute_account_bills_count(self):
        for wizard in self:
            purchase_order_ids = self.env['purchase.order'].search([('ship_id', '=', wizard.id)])
            wizard.account_bills_count = len(purchase_order_ids.mapped('invoice_ids'))

    is_ship = fields.Boolean(string="Ship", default=False)
    incoming_picking_count = fields.Integer(string="Incoming Picking", compute="_compute_incoming_picking_count")
    outgoing_picking_count = fields.Integer(string="Outgoing Picking", compute="_compute_outgoing_picking_count")
    crew_ids = fields.One2many('res.partner', 'parent_id', string='Crew', domain=[
                               ('active', '=', True), ('type', '=', 'crew')])
    account_invoice_count = fields.Integer(string="Stock Picking", compute="_compute_account_invoice_count")
    account_bills_count = fields.Integer(string="Stock Picking", compute="_compute_account_bills_count")
    ship_sale_order_count = fields.Integer(string="Ship Sale Order", compute="_compute_ship_sale_order_count")
    ship_stay_count = fields.Integer(string="Stay", compute="_compute_ship_stay_count")
    ship_expenses_count= fields.Integer(string="Ship Expenses", compute="_compute_ship_expense_count")
    purchase_order_count = fields.Integer(
        compute="_compute_purchase_order_count", string="Purchase Order")
    type = fields.Selection(
        [('contact', 'Contact'),
         ('invoice', 'Invoice address'),
         ('delivery', 'Shipping address'),
         ('other', 'Other address'),
         ('crew', 'Crew')], string='Address Type',
        default='contact',
        help="Used to select automatically the right address according to the context in sales and purchases documents.")

    
    def return_model_ids(self,model,account_type):
        if account_type == 'purchase':
            purchase_order_ids = self.env['purchase.order'].search([('ship_id', '=', self.id)])
            if model == "account.invoice":
                records = purchase_order_ids.mapped('invoice_ids').ids
            else:
                records = purchase_order_ids.mapped('picking_ids').ids
        if account_type == 'sale':
            sale_order_ids = self.env['sale.order'].search([('ship_id', '=', self.id)])
            if model == "account.invoice":
                records = sale_order_ids.mapped('invoice_ids').ids
            else:
                records = sale_order_ids.mapped('picking_ids').ids
        return records

    @api.multi
    def res_partner_2_other_model(self):
        # smart buttons method which returns diffrent model views.
        model = self._context.get('from')
        model_name = ' '.join(model.split('.'))
        account_type = self._context.get('account_type')
        if account_type:
            model_ids = self.return_model_ids(model,account_type)
            return  {
                'type': 'ir.actions.act_window',
                'res_model': model,
                'name' : model_name,
                'view_mode': 'tree,form',
                'view_type': 'form',
                'domain': [('id','in', model_ids)],
                'target': 'self',
            }  
        else:  
            return  {
                'type': 'ir.actions.act_window',
                'res_model': model,
                'name' : model_name,
                'view_mode': 'tree,form',
                'view_type': 'form',
                'domain': [('ship_id','=', self.id)],
                'target': 'self',
            }

    @api.multi
    def write(self, vals):
        if self.is_ship:
            if self.ship_sale_order_count > 0 or self.purchase_order_count > 0:
                if 'is_ship' in vals:
                    raise UserError(_("You can not edit 'ship' field.This record related to sales orders and purchase order.")) 
        return super(ResPartner, self).write(vals)

        
