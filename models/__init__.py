# -*- coding: utf-8 -*-

from . import ship_stay
from . import hr_expense
from . import purchase_order
from . import res_partner
from . import sale_order
from . import stock_picking
from . import terms_conditions
from . import account_invoice