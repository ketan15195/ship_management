# -*- coding: utf-8 -*-

from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = "stock.picking"

    ship_id = fields.Many2one('res.partner', string="Ship")
    ship_stay_ids = fields.One2many('ship.stay','stock_picking_id')


    @api.multi
    def action_cancel(self):
        if not self.sale_id:
    		purchase_order_id = self.env['purchase.order'].search([('group_id', '=',self.group_id.id)])
    		for move_line in self.move_lines:
    			order_line = purchase_order_id.order_line.filtered(lambda order:order.product_id == move_line.product_id)
    			order_line.cancelled_units = move_line.product_uom_qty
        res = super(StockPicking, self).action_cancel()
        return res