# -*- coding: utf-8 -*-

from odoo import api, fields, models


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    ship_id = fields.Many2one('res.partner', string="Ship", required=True, domain=[
                              ('is_ship', '=', True)])
    stay_ids = fields.One2many('ship.stay', 'purchase_order_id')
    responsible_employee_id = fields.Many2one(
        'hr.employee', string="Responsible")

    @api.model
    def _prepare_picking(self):
        # Adds ship_id in stock.picking record when purchase order is
        # confirmed.
        values = super(PurchaseOrder, self)._prepare_picking()
        values.update({
            'ship_id': self.ship_id.id
        })
        return values


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    cancelled_units = fields.Integer(string="Cancelled Qty", readonly="True")    