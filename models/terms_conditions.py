# -*- coding: utf-8 -*-

from odoo import fields, models

class Terms_Conditions(models.Model):
	_name = "terms_conditions"

	name = fields.Char()
	sale_order_id = fields.Many2one('sale.order')
	notes = fields.Html()