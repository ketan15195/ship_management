# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HrExpense(models.Model):
    _inherit = "hr.expense"

    ship_id = fields.Many2one('res.partner', string="Ship")
    stay_ids = fields.One2many('ship.stay', 'hr_expense_id')

    @api.onchange('sale_order_id')
    def _onchange_sale_order(self):
    	self.ship_id = self.sale_order_id.ship_id