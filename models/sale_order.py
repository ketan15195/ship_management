# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    ship_id = fields.Many2one('res.partner', string="Ship", required=True, domain=[('is_ship', '=', True)])
    terms_condition_id = fields.One2many('terms_conditions','sale_order_id', string="Terms and Condition")
    notes = fields.Html()
    print_vat = fields.Boolean()
    print_unit_price = fields.Boolean()
    print_sub_total = fields.Boolean()
    print_total_amount = fields.Boolean()
    ship_stay_ids = fields.One2many('ship.stay','sale_order_id')
    subject = fields.Char(string="Subject")


    @api.multi
    def action_confirm(self):
    	# Adds ship_id in stock.picking record which is related to sale order
        res = super(SaleOrder, self).action_confirm()
        for picking_id in self.picking_ids:
        	picking_id.ship_id = self.ship_id
        return res